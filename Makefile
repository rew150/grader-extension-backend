build:
	go build -o ./dist/bin/main .

prep-start:
	chmod +x ./dist/bin/main

start: prep-start
	./dist/bin/main

start-prod: prep-start
	export GIN_MODE=release; \
	./dist/bin/main

run:
	go run .

run-prod: build prep-start
	export GIN_MODE=release; \
	./dist/bin/main

test:
	go test ./...
