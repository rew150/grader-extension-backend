package submission

type Submission struct {
	EpochNano     int64
	ID            string
	PublicKey     string
	SignedCode    string
	TransactionID string
}
