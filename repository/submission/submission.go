package submission

import (
	"gitlab.com/rew150/grader-extension-backend/database"
)

func Save(submission Submission) error {
	query := `INSERT INTO submission
	(epoch_nano, id, public_key, signed_code, transaction_id)
	VALUES ($1, $2, $3, $4, $5);`

	_, err := database.DbPool.Exec(database.Context, query,
		submission.EpochNano,
		submission.ID,
		submission.PublicKey,
		submission.SignedCode,
		submission.TransactionID,
	)
	if err != nil {
		return err
	}
	return nil
}

func FindAllSubmission(startEpochNano, endEpochNano int64) ([]Submission, error) {
	query := `SELECT * FROM submission
	WHERE epoch_nano >= $1
	AND epoch_nano <= $2
	GROUP BY id
	ORDER BY epoch_nano;`

	rows, err := database.DbPool.Query(database.Context, query, startEpochNano, endEpochNano)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results []Submission

	for rows.Next() {
		var s Submission
		err = rows.Scan(&s.EpochNano, &s.ID, &s.PublicKey, &s.SignedCode, &s.TransactionID)
		if err != nil {
			return nil, err
		}

		results = append(results, s)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

func FindSubmissionByID(startEpochNano, endEpochNano int64, id string) ([]Submission, error) {
	query := `SELECT * FROM submission
	WHERE epoch_nano >= $1
	AND epoch_nano <= $2
	AND id = $3
	ORDER BY epoch_nano;`

	rows, err := database.DbPool.Query(database.Context, query, startEpochNano, endEpochNano, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results []Submission

	for rows.Next() {
		var s Submission
		err = rows.Scan(&s.EpochNano, &s.ID, &s.PublicKey, &s.SignedCode, &s.TransactionID)
		if err != nil {
			return nil, err
		}

		results = append(results, s)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}
