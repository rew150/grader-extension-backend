package association

type Association struct {
	EpochNano     int64
	ID            string
	PublicKey     string
	TransactionID string
}
