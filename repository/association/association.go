package association

import (
	"gitlab.com/rew150/grader-extension-backend/database"
)

func Save(association Association) error {
	query := `INSERT INTO association
	(epoch_nano, id, public_key, transaction_id)
	VALUES ($1, $2, $3, $4);`

	_, err := database.DbPool.Exec(database.Context, query,
		association.EpochNano,
		association.ID,
		association.PublicKey,
		association.TransactionID,
	)
	if err != nil {
		return err
	}
	return nil
}

func IsLastValidAssociation(id, publicKey string) (ok bool) {
	query := `SELECT * FROM association
	WHERE id = $1
	AND public_key = $2
	AND transaction_id IS NOT NULL
	AND transaction_id <> ''
	ORDER BY epoch_nano DESC
	LIMIT 1;`

	row := database.DbPool.QueryRow(database.Context, query, id, publicKey)

	var a Association
	err := row.Scan(&a.EpochNano, &a.ID, &a.PublicKey, &a.TransactionID)

	ok = err == nil
	return
}

func FindAllAssociation(id string, startEpochNano int64) ([]Association, error) {
	query := `SELECT * FROM association
	WHERE id = $1
	AND epoch_nano >= $2
	ORDER BY epoch_nano DESC;`

	rows, err := database.DbPool.Query(database.Context, query, id, startEpochNano)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results []Association

	for rows.Next() {
		var a Association
		err = rows.Scan(&a.EpochNano, &a.ID, &a.PublicKey, &a.TransactionID)
		if err != nil {
			return nil, err
		}

		results = append(results, a)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

func Exists(id, publicKey string) (bool, error) {
	query := `SELECT COUNT(*) FROM association
	WHERE id = $1
	AND public_key = $2;`

	row := database.DbPool.QueryRow(database.Context, query, id, publicKey)

	var c int
	err := row.Scan(&c)
	if err != nil {
		return false, err
	}

	return c != 0, nil
}

func KeyExists(publicKey string) (bool, error) {
	query := `SELECT COUNT(*) FROM association
	WHERE public_key = $1;`
	row := database.DbPool.QueryRow(database.Context, query, publicKey)
	var c int
	err := row.Scan(&c)
	if err != nil {
		return false, err
	}
	return c != 0, nil
}
