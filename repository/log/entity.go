package log

type Log struct {
	EpochNano   int64
	Latency     int
	Description string
	Path        string
	Status      string
	ID          string
}
