package log

import (
	"gitlab.com/rew150/grader-extension-backend/database"
)

func Write(log Log) error {
	query := `INSERT INTO log
	(epoch_nano, latency, description, path, status, id) VALUES
	($1, $2, $3, $4, $5, $6);`

	_, err := database.DbPool.Exec(database.Context, query,
		log.EpochNano,
		log.Latency,
		log.Description,
		log.Path,
		log.Status,
		log.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

func Read() ([]Log, error) {
	query := `SELECT * FROM log
	ORDER BY epoch_nano DESC
	LIMIT 30;`

	rows, err := database.DbPool.Query(database.Context, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results []Log

	for rows.Next() {
		var l Log
		err = rows.Scan(&l.EpochNano, &l.Latency, &l.Description, &l.Path, &l.Status, &l.ID)
		if err != nil {
			return nil, err
		}

		results = append(results, l)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}
