FROM docker.io/golang:1.15-alpine

WORKDIR /app

RUN apk --no-cache add make

COPY . .

RUN make build

CMD make start-prod
