module gitlab.com/rew150/grader-extension-backend

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jackc/pgx/v4 v4.10.1
	github.com/joho/godotenv v1.3.0
	github.com/manifoldco/promptui v0.8.0
	github.com/stellar/go v0.0.0-20201218182351-013bf861cb0f
	github.com/stretchr/testify v1.6.1
	gitlab.com/wongtawan-j/grader-extension-module v1.0.14
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
