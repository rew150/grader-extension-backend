package merkletree

import (
	"crypto/sha256"
	"encoding/hex"
	"hash"
)

type Node struct {
	Digest string
	Item   string
	Left   *Node
	Right  *Node
}

type Element interface {
	Node_ify() (*Node, error)
}

type MerkleTree []Element

type StringElement string

type TreeElement MerkleTree

func Hasher() hash.Hash {
	return sha256.New()
}

func Hash(s string) string {
	hasher := Hasher()
	hasher.Write([]byte(s))

	res := hasher.Sum(nil)

	return hex.EncodeToString(res)
}

func DigestToByteArray(s string) ([32]byte, error) {
	var a [32]byte
	ba, err := hex.DecodeString(s)
	if err != nil {
		return a, err
	}
	if len(ba) != 32 {
		return a, DigestNot32ByteError{}
	}
	copy(a[:], ba)
	return a, nil
}
