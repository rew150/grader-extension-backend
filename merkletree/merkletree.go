package merkletree

func (s StringElement) Node_ify() (*Node, error) {
	v := string(s)
	n := &Node{
		Digest: Hash(v),
		Item:   v,
		Left:   nil,
		Right:  nil,
	}
	return n, nil
}

func (t TreeElement) Node_ify() (*Node, error) {
	return MerkleTree(t).SyncMerkleDigest()
}

func (t MerkleTree) SyncMerkleDigest() (*Node, error) {
	if len(t) == 0 {
		return nil, NoElement{}
	}

	currentList := make([]*Node, len(t), len(t)+1)
	for i, e := range t {
		n, err := e.Node_ify()
		if err != nil {
			return nil, err
		}

		currentList[i] = n
	}

	for len(currentList) > 1 {
		if len(currentList)&1 == 1 {
			currentList = append(currentList, currentList[len(currentList)-1])
		}

		for i := 0; i < len(currentList); i += 2 {
			left := currentList[i]
			right := currentList[i+1]
			item := left.Digest + right.Digest
			digest := Hash(item)

			currentList[i/2] = &Node{
				Digest: digest,
				Item:   item,
				Left:   left,
				Right:  right,
			}
		}

		currentList = currentList[:len(currentList)/2]
	}

	return currentList[0], nil
}
