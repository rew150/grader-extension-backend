package merkletree

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestStringElement_Nodeify(t *testing.T) {
	var s StringElement = "this is some random string"
	digest := "9091604d9885cfaeb18f0eaadaaa7c98a0a2f10b13371ce1b7dcadb17abe06f0"

	node, err := s.Node_ify()

	assert.Nil(t, err)
	assert.Equal(t, node.Digest, digest)
	assert.Equal(t, string(s), node.Item)
	assert.Nil(t, node.Left)
	assert.Nil(t, node.Right)
}

// assume TestStringElement_Nodeify is passed
func TestMerkleTree_SyncMerkleDigest_OnlyStringElement(t *testing.T) {
	var s1 StringElement = "this is some random string one"
	var s2 StringElement = "this is some random string two"

	nodeS1, err1 := s1.Node_ify()
	nodeS2, err2 := s2.Node_ify()
	assert.Nil(t, err1)
	assert.Nil(t, err2)

	merkleTree := MerkleTree{s1, s2}
	result, err := merkleTree.SyncMerkleDigest()

	assert.Nil(t, err)

	assert.Equal(t, &Node{
		Digest: Hash(nodeS1.Digest + nodeS2.Digest),
		Item:   nodeS1.Digest + nodeS2.Digest,
		Left:   nodeS1,
		Right:  nodeS2,
	}, result)
}

// assume TestStringElement_Nodeify is passed
func TestMerkleTree_SyncMerkleDigest_OnlyStringElement_SingleElement(t *testing.T) {
	var s1 StringElement = "this is some random string one"

	nodeS1, err1 := s1.Node_ify()
	assert.Nil(t, err1)

	merkleTree := MerkleTree{s1}
	result, err := merkleTree.SyncMerkleDigest()

	assert.Nil(t, err)
	assert.Equal(t, nodeS1, result)
}

// assume TestStringElement_Nodeify is passed
func TestMerkleTree_SyncMerkleDigest_OnlyStringElement_RequireExtension(t *testing.T) {
	var s1 StringElement = "this is some random string one"
	var s2 StringElement = "this is some random string two"
	var s3 StringElement = "this is some random string three"

	nodeS1, err1 := s1.Node_ify()
	nodeS2, err2 := s2.Node_ify()
	nodeS3, err3 := s3.Node_ify()
	assert.Nil(t, err1)
	assert.Nil(t, err2)
	assert.Nil(t, err3)

	merkleTree := MerkleTree{s1, s2, s3}
	result, err := merkleTree.SyncMerkleDigest()

	assert.Nil(t, err)

	leftNode := &Node{
		Digest: Hash(nodeS1.Digest + nodeS2.Digest),
		Item:   nodeS1.Digest + nodeS2.Digest,
		Left:   nodeS1,
		Right:  nodeS2,
	}

	rightNode := &Node{
		Digest: Hash(nodeS3.Digest + nodeS3.Digest),
		Item:   nodeS3.Digest + nodeS3.Digest,
		Left:   nodeS3,
		Right:  nodeS3,
	}

	assert.Equal(t, &Node{
		Digest: Hash(leftNode.Digest + rightNode.Digest),
		Item:   leftNode.Digest + rightNode.Digest,
		Left:   leftNode,
		Right:  rightNode,
	}, result)
}

// assume TestStringElement_Nodeify is passed
func TestMerkleTree_SyncMerkleDigest_MultiElementType(t *testing.T) {
	var s1 StringElement = "this is some random string one"
	var s2 StringElement = "this is some random string two"

	nodeS1, err1 := s1.Node_ify()
	nodeS2, err2 := s2.Node_ify()
	assert.Nil(t, err1)
	assert.Nil(t, err2)

	merkleTreeS1 := TreeElement{s1}

	merkleTree := MerkleTree{merkleTreeS1, s2}
	result, err := merkleTree.SyncMerkleDigest()
	assert.Nil(t, err)
	assert.Equal(t, &Node{
		Digest: Hash(nodeS1.Digest + nodeS2.Digest),
		Item:   nodeS1.Digest + nodeS2.Digest,
		Left:   nodeS1,
		Right:  nodeS2,
	}, result)
}
