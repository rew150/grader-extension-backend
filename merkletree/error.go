package merkletree

type NoElement struct{}

func (e NoElement) Error() string {
	return "No element exist in the list, could't digest."
}

type DigestNot32ByteError struct{}

func (e DigestNot32ByteError) Error() string {
	return "Digest has length that is not 32"
}
