package util

import (
	"fmt"
	"log"
	"os"
	"time"

	Log "gitlab.com/rew150/grader-extension-backend/repository/log"

	"github.com/manifoldco/promptui"
	"github.com/stellar/go/clients/horizonclient"
)

func PromptYesNo() bool {
	prompt := promptui.Select{
		Label: "Select[Yes/No]",
		Items: []string{"Yes", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}

// UseLogger Auto-fill the EpochNano and latency in milliseconds
func UseLogger(path string) (GetLog func(status, id, description string, err error) error) {
	startTime := time.Now()

	GetLog = func(status, id, description string, err error) error {
		now := time.Now()
		l := Log.Log{
			EpochNano:   now.UnixNano(),
			Latency:     int(now.Sub(startTime).Milliseconds()),
			Description: description,
			Path:        path,
			Status:      status,
			ID:          id,
		}
		if err != nil {
			switch v := err.(type) {
			case *horizonclient.Error:
				fmt.Fprintln(os.Stderr, v.Problem.Extras["result_codes"])
			default:
				fmt.Fprintln(os.Stderr, err)
			}
		}
		return Log.Write(l)
	}
	return
}
