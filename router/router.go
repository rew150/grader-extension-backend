package router

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/rew150/grader-extension-backend/hello"
)

func Router() (r *gin.Engine) {
	r = gin.Default()

	r.GET("/hello", hello.GetHello)

	r.Static("/admin", "./dist/public")

	return
}
