# Grader Extension Backend (Revised)

Revised version of grader extension backend (formerly written in express.js)

This project make use of `go mod`. So, please pay an attention to it.

`Dockerfile` is only used for production deployment, other information is self-documented in `docker-compose.yaml`

# Deploy Backend

```shell
gcloud compute ssh application --command="mkdir /home/application/backend"
gcloud compute scp --recurse ./deploy-backend/* application:/home/application/backend
```

# Makefile explanation
#### build

build the app and place binary in dist/bin

#### start

run the executable file dist/bin/main

#### start-prod

run the executable file dist/bin/main with gin release mode

#### run

just basic `go run .`

#### run-prod

`go run .` with gin release mode

#### test

test all package (some are not implemented)
