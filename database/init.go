package database

import (
	"context"
	"fmt"
	"log"
	"net/url"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/rew150/grader-extension-backend/config"
)

var (
	Context context.Context
	DbPool  *pgxpool.Pool
)

func init() {
	err := TryOpen()
	if err != nil {
		log.Fatalln("[database/init] Can't connect to db ...")
	}
}

func TryOpen() error {
	if DbPool == nil {
		Context = context.Background()
		connStr := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s",
			url.PathEscape(config.Config.DbUser),
			url.PathEscape(config.Config.DbPassword),
			url.PathEscape(config.Config.DbHost),
			"5432",
			url.PathEscape(config.Config.DbName),
		)

		var err error
		DbPool, err = pgxpool.Connect(Context, connStr)
		if err != nil {
			return err
		}

		var greeting string
		err = DbPool.QueryRow(Context, "SELECT 'Hello, world!'").Scan(&greeting)
		if err != nil {
			return err
		}

		fmt.Println("[database/init]", greeting)
	}
	return nil
}
