CREATE EXTENSION IF NOT EXISTS timescaledb;

CREATE TABLE IF NOT EXISTS association (
    epoch_nano      BIGINT      NOT NULL,
    id              TEXT        NOT NULL,
    public_key      TEXT        NOT NULL,
    transaction_id  TEXT        NULL
);

SELECT create_hypertable('association', 'epoch_nano', chunk_time_interval => 86400000000000);

CREATE TABLE IF NOT EXISTS submission (
    epoch_nano      BIGINT      NOT NULL,
    id              TEXT        NOT NULL,
    public_key      TEXT        NOT NULL,
    signed_code     TEXT        NOT NULL,
    transaction_id  TEXT        NULL
);

SELECT create_hypertable('submission', 'epoch_nano', chunk_time_interval => 86400000000000);


CREATE TABLE IF NOT EXISTS log (
    epoch_nano   BIGINT      NOT NULL,
    latency      INTEGER     NULL,
    description  TEXT        NULL,
    path         TEXT        NULL,
    status       TEXT        NULL,
    id           TEXT        NULL
);

SELECT create_hypertable('log', 'epoch_nano', chunk_time_interval => 86400000000000);
