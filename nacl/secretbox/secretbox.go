package secretbox

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"

	"golang.org/x/crypto/nacl/secretbox"
)

func hash(s string) *[32]byte {
	hasher := sha256.New()
	hasher.Write([]byte(s))

	var arr [32]byte

	copy(arr[:], hasher.Sum(nil))

	return &arr
}

func Encrypt(code, sessionKey string) string {
	var nonce [24]byte
	rand.Read(nonce[:])
	res := secretbox.Seal(nonce[:], []byte(code), &nonce, hash(sessionKey))
	return base64.StdEncoding.EncodeToString(res)
}

func Decrypt(base64Box string, sessionKey string) (string, bool) {
	box, err := base64.StdEncoding.DecodeString(base64Box)
	if err != nil {
		return "", false
	}

	var nonce [24]byte
	copy(nonce[:], box[:24])
	code, ok := secretbox.Open(nil, box[24:], &nonce, hash(sessionKey))
	if !ok {
		return "", false
	}
	return string(code), true
}
