package secretbox_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/rew150/grader-extension-backend/nacl/secretbox"
)

const (
	code = `
for int i = 1 to 10:
	i++
print(i)
end of program`

	sessionKey = "aaaaBBBccCCdD019438"

	box = "276TSiLsrCtsC/vQqtHBr1TW5YHudIXyLaiS1fsTxnfB3BjL0kVRIBja8EBfBVS6srO11Jv/3qMk2ji5ucvIuaQYOtt6mEqnwxl91LJqao9p6ptHRnY4YF4u"
)

func TestEncryptDecryptConsistent(t *testing.T) {
	assert := assert.New(t)

	box := secretbox.Encrypt(code, sessionKey)
	res, ok := secretbox.Decrypt(box, sessionKey)

	assert.True(ok, "Decrypt must not error")
	assert.Equal(code, res, "Result must be the same code")
}

func TestDecryptBorkedBase64(t *testing.T) {
	assert := assert.New(t)

	borkedBase64 := "fd=sfsd.014"

	_, ok := secretbox.Decrypt(borkedBase64, sessionKey)

	assert.False(ok, "Decrypt must error in case of borked base64")
}

func TestDecryptWrongSessionKey(t *testing.T) {
	assert := assert.New(t)

	borkedSessionKey := sessionKey + "x"
	_, ok := secretbox.Decrypt(box, borkedSessionKey)

	assert.False(ok, "Decrypt must error in case of borked key")
}
