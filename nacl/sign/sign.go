package sign

import (
	"encoding/base64"

	"golang.org/x/crypto/nacl/sign"
)

func Sign(code, base64privateKey string) (string, bool) {

	privateKey, err := base64.StdEncoding.DecodeString(base64privateKey)
	if err != nil || len(privateKey) != 64 {
		return "", false
	}
	var privateA [64]byte
	copy(privateA[:], privateKey)

	res := sign.Sign(nil, []byte(code), &privateA)
	return base64.StdEncoding.EncodeToString(res), true
}

func Open(signedCode, base64publicKey string) (string, bool) {
	signedCodeB, err := base64.StdEncoding.DecodeString(signedCode)
	if err != nil {
		return "", false
	}

	publicKey, err := base64.StdEncoding.DecodeString(base64publicKey)
	if err != nil || len(publicKey) != 32 {
		return "", false
	}
	var publicA [32]byte
	copy(publicA[:], publicKey)

	codeB, ok := sign.Open(nil, signedCodeB, &publicA)
	if ok {
		return string(codeB), true
	}
	return "", false
}
