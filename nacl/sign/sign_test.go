package sign_test

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	s "gitlab.com/rew150/grader-extension-backend/nacl/sign"
	"golang.org/x/crypto/nacl/sign"
)

const (
	code = `
for int i = 1 to 10:
	i++
print(i)
end of program`
)

func TestSignOpenConsistent(t *testing.T) {
	assert := assert.New(t)

	pub, priv, err := sign.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal("Couldn't sign_test due to unexpected problem")
	}

	box, ok := s.Sign(code, base64.StdEncoding.EncodeToString((*priv)[:]))
	assert.True(ok, "sign must be ok with std base64 generated priv")

	outCode, ok := s.Open(box, base64.StdEncoding.EncodeToString((*pub)[:]))
	assert.True(ok, "open must be ok with std base64 generated pub")

	assert.Equal(code, outCode, "Code must be consistent")
}

func TestSignFailInvalidLengthPriv(t *testing.T) {
	assert := assert.New(t)

	_, priv, err := sign.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal("Couldn't sign_test due to unexpected problem")
	}

	privSlice := make([]byte, 65)
	copy(privSlice[:64], priv[:])

	_, ok := s.Sign(code, base64.StdEncoding.EncodeToString(privSlice))
	assert.False(ok, "Priv key invalid (length > 64) must fail")

	privSlice = privSlice[:63]

	_, ok = s.Sign(code, base64.StdEncoding.EncodeToString(privSlice))
	assert.False(ok, "Priv key invalid (length < 64) must fail")
}

func TestOpenFailInvalidLengthPub(t *testing.T) {
	assert := assert.New(t)

	pub, priv, err := sign.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal("Couldn't sign_test due to unexpected problem")
	}

	box, ok := s.Sign(code, base64.StdEncoding.EncodeToString((*priv)[:]))
	assert.True(ok, "sign must be ok with std base64 generated priv")

	pubSlice := make([]byte, 65)
	copy(pubSlice[:64], pub[:])

	_, ok = s.Open(box, base64.StdEncoding.EncodeToString(pubSlice))
	assert.False(ok, "Pub key invalid (length > 64) must fail")

	pubSlice = pubSlice[:63]

	_, ok = s.Open(box, base64.StdEncoding.EncodeToString(pubSlice))
	assert.False(ok, "Pub key invalid (length < 64) must fail")
}

func TestOpenFailPubNotAlignedPriv(t *testing.T) {
	assert := assert.New(t)

	pub, priv, err := sign.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal("Couldn't sign_test due to unexpected problem")
	}

	box, ok := s.Sign(code, base64.StdEncoding.EncodeToString((*priv)[:]))
	assert.True(ok, "sign must be ok with std base64 generated priv")

	pubSlice := make([]byte, 64)
	if sliceEqual(pubSlice, pub[:]) {
		pubSlice[0] = byte(1)
	}

	_, ok = s.Open(box, base64.StdEncoding.EncodeToString(pubSlice))
	assert.False(ok, "Unaligned Pub/Priv must fail")
}

func sliceEqual(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
