package stellar

import (
	"fmt"
	"log"

	hClient "github.com/stellar/go/clients/horizonclient"
	"github.com/stellar/go/keypair"
	"github.com/stellar/go/protocols/horizon"
	"github.com/stellar/go/txnbuild"
	"gitlab.com/rew150/grader-extension-backend/config"
	"gitlab.com/rew150/grader-extension-backend/util"
)

var (
	stellarServer *hClient.Client
	graderKeypair *keypair.Full
	graderAccount *horizon.Account
	txnOps        []txnbuild.Operation
)

func init() {
	stellarServer = getStellarServer()
	graderKeypair = genGraderKeypair()
	graderAccount = getGraderAccount(graderKeypair.Address(), stellarServer)
	txnOps = genTxnOps(graderKeypair.Address())

	fmt.Println("[stellar/init] \"stellar\" instantiated.")
}

func getStellarServer() (s *hClient.Client) {
	if config.Config.IsUsingPublicNetwork {
		s = hClient.DefaultPublicNetClient
	} else {
		s = hClient.DefaultTestNetClient
	}
	fmt.Println("[stellar/init] stellar server got")
	return
}

func genGraderKeypair() (k *keypair.Full) {
	k = keypair.MustParseFull(config.Config.SecretKey)
	fmt.Println("[stellar/init] grader keypair got")
	return
}

func getGraderAccount(addr string, stellar *hClient.Client) *horizon.Account {
	var graderAcc horizon.Account
	var err error
	funded := false

	for {
		graderAcc, err = stellar.AccountDetail(hClient.AccountRequest{AccountID: addr})

		if err != nil {
			if !funded && !config.Config.IsUsingPublicNetwork {
				fmt.Println("[stellar/init] There's an error getting grader account, maybe the account was not funded, try funding? ")

				tryfund := util.PromptYesNo()
				if tryfund {
					stellar.Fund(addr)
					funded = true
					continue
				}
			}

			if funded {
				fmt.Println("[stellar/init] Funding doesn't solve the problem.")
			}
			log.Fatalln(err)
		}

		break
	}

	fmt.Println("[stellar/init] grader account got")
	return &graderAcc
}

func genTxnOps(addr string) (ops []txnbuild.Operation) {
	ops = []txnbuild.Operation{
		&txnbuild.Payment{
			Destination: addr,
			Asset:       txnbuild.NativeAsset{},
			Amount:      "1",
		},
	}
	fmt.Println("[stellar/init] txnops got")
	return
}
