package stellar

import (
	"github.com/stellar/go/network"
	"github.com/stellar/go/txnbuild"
	"gitlab.com/rew150/grader-extension-backend/config"
)

// return txnid if success
func SyncSaveMemo(hash [32]byte) (string, error) {
	txn, err := txnbuild.NewTransaction(
		txnbuild.TransactionParams{
			SourceAccount:        graderAccount,
			IncrementSequenceNum: true,
			Operations:           txnOps,
			BaseFee:              txnbuild.MinBaseFee,
			Timebounds:           txnbuild.NewTimeout(60),
			Memo:                 txnbuild.MemoHash(hash),
		},
	)
	if err != nil {
		return "", err
	}

	var netParsephrase string
	if config.Config.IsUsingPublicNetwork {
		netParsephrase = network.PublicNetworkPassphrase
	} else {
		netParsephrase = network.TestNetworkPassphrase
	}

	txn, err = txn.Sign(netParsephrase, graderKeypair)
	if err != nil {
		return "", err
	}

	txe, err := txn.Base64()
	if err != nil {
		return "", err
	}

	res, err := stellarServer.SubmitTransactionXDR(txe)
	if err != nil {
		return "", err
	}

	if !res.Successful {
		return "", TransactionNotSuccessful{}
	}

	return res.ID, nil
}

func AsyncSaveMemo(hash [32]byte, resChan chan<- string, errChan chan<- error) {
	go func(hash [32]byte) {
		txnid, err := SyncSaveMemo(hash)
		if err != nil {
			errChan <- err
		} else {
			resChan <- txnid
		}
	}(hash)
}
