package stellar

type TransactionNotSuccessful struct{}

func (e TransactionNotSuccessful) Error() string {
	return "Transaction not successful"
}
