package config

import (
	"fmt"
	"log"
	"os"
	"strings"
)

type Struct struct {
	IsUsingPublicNetwork bool
	SecretKey            string

	DbHost     string
	DbUser     string
	DbPassword string
	DbName     string
}

var Config *Struct

func printConfig() {
	fmt.Println("[config/init] application configuration")
	fmt.Printf("\t- UsingPublicNetwork: %t\n", Config.IsUsingPublicNetwork)

	if secretlen := len(Config.SecretKey); secretlen <= 20 {
		fmt.Printf("\t- SecretKey: %s (masked)\n", strings.Repeat("*", secretlen))
	} else {
		fmt.Printf("\t- SecretKey: %s... and so on\n", Config.SecretKey[0:10])
	}

	fmt.Printf("\t- DbHost: %s\n", Config.DbHost)
	fmt.Printf("\t- DbUser: %s\n", Config.DbUser)

	if secretlen := len(Config.DbPassword); secretlen <= 10 {
		fmt.Printf("\t- DbPassword: %s (masked)\n", strings.Repeat("*", secretlen))
	} else {
		fmt.Printf("\t- DbPassword: %s... and so on\n", Config.DbPassword[0:5])
	}

	fmt.Printf("\t- DbName: %s\n", Config.DbName)
}

func init() {
	Config = &Struct{
		IsUsingPublicNetwork: os.Getenv("USE_PUBLIC_NETWORK") == "1",
		SecretKey:            os.Getenv("STELLAR_SECRET_KEY"),
		DbHost:               os.Getenv("POSTGRES_HOST"),
		DbUser:               os.Getenv("POSTGRES_USER"),
		DbPassword:           os.Getenv("POSTGRES_PASSWORD"),
		DbName:               os.Getenv("POSTGRES_DB"),
	}

	if Config.SecretKey == "" {
		log.Fatalln("STELLAR_SECRET_KEY not specified, exiting the app...")
	}

	fmt.Println("[config/init] \"Config\" instantiated.")
}
