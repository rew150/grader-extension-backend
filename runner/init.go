package runner

import (
	"log"

	"gitlab.com/rew150/grader-extension-backend/runner/handler/association"
	"gitlab.com/rew150/grader-extension-backend/runner/handler/submission"
	"gitlab.com/wongtawan-j/grader-extension-module/kafka"
)

func init() {
	associationMessages := make(chan []byte)
	go kafka.Consume(kafka.AssociationPartition, associationMessages)

	submissionMessages := make(chan []byte)
	go kafka.Consume(kafka.SubmissionPartition, submissionMessages)

	go func() {
		log.Println("start consume partitions")
		for {
			select {
			case message := <-associationMessages:
				err := association.AssociationHandler(message)
				if err != nil {
					kafka.ProduceAssociationMessage(message)
				}
			default:
				select {
				case message := <-associationMessages:
					err := association.AssociationHandler(message)
					if err != nil {
						kafka.ProduceAssociationMessage(message)
					}
				case message := <-submissionMessages:
					err := submission.SubmissionHandler(message)
					if err != nil {
						kafka.ProduceSubmissionMessage(message)
					}
				}
			}
		}
	}()
}
