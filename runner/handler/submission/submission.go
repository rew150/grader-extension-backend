package submission

import (
	"encoding/base64"
	"fmt"

	m "gitlab.com/rew150/grader-extension-backend/merkletree"
	"gitlab.com/rew150/grader-extension-backend/nacl/sign"
	"gitlab.com/rew150/grader-extension-backend/repository/association"
	"gitlab.com/rew150/grader-extension-backend/repository/submission"
	"gitlab.com/rew150/grader-extension-backend/stellar"
	"gitlab.com/rew150/grader-extension-backend/util"
	"gitlab.com/wongtawan-j/grader-extension-module/message"
)

func SubmissionHandler(msg []byte) error {
	logger := util.UseLogger(":submission")
	subm := message.Submission{}
	subm.Deserialize(msg)

	_, ok := sign.Open(subm.SignedCode, subm.PublicKey)
	if !ok {
		logger(
			"err",
			subm.StudentID,
			"signedCode="+subm.SignedCode+
				"; publicKey="+subm.PublicKey+
				"; can't open code with publicKey",
			nil,
		)
		// this error is not retryable
		return nil
	}

	ok = association.IsLastValidAssociation(subm.StudentID, subm.PublicKey)
	if !ok {
		if exist, err := association.Exists(subm.StudentID, subm.PublicKey); err != nil {
			logger(
				"err",
				subm.StudentID,
				"publicKey="+subm.PublicKey+
					"; unexpected association.exists error",
				err,
			)
			// this error is not retryable
			return nil
		} else if !exist {
			logger(
				"err",
				subm.StudentID,
				"publicKey="+subm.PublicKey+
					"; association not exists",
				nil,
			)
			// this error is not retryable
			return nil
		} else {
			logger(
				"war",
				subm.StudentID,
				"publicKey="+subm.PublicKey+
					"; association not latest",
				nil,
			)
		}
	}

	mkt := m.MerkleTree([]m.Element{
		m.StringElement(fmt.Sprint(subm.EpochNano)),
		m.StringElement(subm.StudentID),
		m.StringElement(subm.PublicKey),
		m.StringElement(subm.SignedCode),
	})
	mktDigest, err := mkt.SyncMerkleDigest()
	if err != nil {
		logger("err", subm.StudentID, "merkle tree unexpected error", err)
		// this error is not retryable
		return nil
	}

	ba, err := m.DigestToByteArray(mktDigest.Digest)
	if err != nil {
		logger("err", subm.StudentID, "unexpected wrong format digest", err)
		// this error is not retryable
		return nil
	}
	txnID, err := stellar.SyncSaveMemo(ba)
	if err != nil {
		logger(
			"err",
			subm.StudentID,
			"byteArray="+base64.StdEncoding.EncodeToString(ba[:])+
				"; error stellar save",
			err,
		)
		// this error "might" be retryable
		return err
	}

	dbSubm := submission.Submission{
		EpochNano:     subm.EpochNano,
		ID:            subm.StudentID,
		PublicKey:     subm.PublicKey,
		SignedCode:    subm.SignedCode,
		TransactionID: txnID,
	}
	err = submission.Save(dbSubm)
	if err != nil {
		logger(
			"err",
			subm.StudentID,
			"epochNano="+fmt.Sprint(dbSubm.EpochNano)+
				"; publicKey="+dbSubm.PublicKey+
				"; signedCode="+dbSubm.SignedCode+
				"; txnID="+dbSubm.TransactionID+
				"; error save after stellar",
			err,
		)
		// this error is not retryable
		return nil
	}

	logger(
		"suc",
		subm.StudentID,
		"epochNano="+fmt.Sprint(dbSubm.EpochNano)+
			"; publicKey="+dbSubm.PublicKey+
			"; signedCode="+dbSubm.SignedCode+
			"; txnID="+dbSubm.TransactionID+
			"; success",
		nil,
	)
	return nil
}
