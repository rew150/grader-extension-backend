package association

import (
	"encoding/base64"
	"fmt"

	m "gitlab.com/rew150/grader-extension-backend/merkletree"
	"gitlab.com/rew150/grader-extension-backend/repository/association"
	"gitlab.com/rew150/grader-extension-backend/stellar"
	"gitlab.com/rew150/grader-extension-backend/util"
	"gitlab.com/wongtawan-j/grader-extension-module/message"
)

func AssociationHandler(msg []byte) error {
	logger := util.UseLogger(":association")

	assoc := message.Association{}
	assoc.Deserialize(msg)

	if exist, err := association.KeyExists(assoc.PublicKey); err != nil {
		logger(
			"err",
			assoc.StudentID,
			"publicKey="+assoc.PublicKey+
				"; unexpected association.KeyExists error",
			err,
		)
		// this error is not retryable
		return nil
	} else if exist {
		logger(
			"err",
			assoc.StudentID,
			"publicKey="+assoc.PublicKey+
				"; public key already exists",
			nil,
		)
		// this error is not retryable
		return nil
	}

	dbAssoc := association.Association{
		EpochNano: assoc.EpochNano,
		ID:        assoc.StudentID,
		PublicKey: assoc.PublicKey,
	}

	err := association.Save(dbAssoc)
	if err != nil {
		logger(
			"err",
			assoc.StudentID,
			"epochNano="+fmt.Sprint(dbAssoc.EpochNano)+
				"; publicKey="+dbAssoc.PublicKey+
				"; error save before stellar",
			err,
		)
		// this error "might" be retryable
		return err
	}

	mkt := m.MerkleTree([]m.Element{
		m.StringElement(fmt.Sprint(assoc.EpochNano)),
		m.StringElement(assoc.StudentID),
		m.StringElement(assoc.PublicKey),
	})
	mktDigest, err := mkt.SyncMerkleDigest()
	if err != nil {
		logger("err", assoc.StudentID, "merkle tree unexpected error", err)
		// this error is not retryable
		return nil
	}

	ba, err := m.DigestToByteArray(mktDigest.Digest)
	if err != nil {
		logger("err", assoc.StudentID, "unexpected wrong format digest", err)
		// this error is not retryable
		return nil
	}
	txnID, err := stellar.SyncSaveMemo(ba)
	if err != nil {
		logger(
			"err",
			assoc.StudentID,
			"byteArray="+base64.StdEncoding.EncodeToString(ba[:])+
				"; error stellar save",
			err,
		)
		// this error "might" be retryable
		return err
	}

	dbAssoc.TransactionID = txnID
	err = association.Save(dbAssoc)
	if err != nil {
		logger(
			"err",
			assoc.StudentID,
			"epochNano="+fmt.Sprint(dbAssoc.EpochNano)+
				"; publicKey="+dbAssoc.PublicKey+
				"; txnID="+dbAssoc.TransactionID+
				"; error save after stellar",
			err,
		)
		// this error is not retryable
		return nil
	}

	logger(
		"suc",
		assoc.StudentID,
		"epochNano="+fmt.Sprint(dbAssoc.EpochNano)+
			"; publicKey="+dbAssoc.PublicKey+
			"; txnID="+dbAssoc.TransactionID+
			"; success",
		nil,
	)
	return nil

}
